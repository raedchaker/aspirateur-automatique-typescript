the project was developed using TypeScript then compiled using tsc ECMAScript 6 with the command tsc --target es6 test.ts
La page web index.html contient un formulaire pour donner les dimensions de la grille et la position de l'aspirateur et son orientation et un input pour les instructions. le résultat sera affiché dans la page aprés l'appui sur le bouton calculer position.

les dimensions de la grille (input 1 et 2 ) doivent être des entiers, la position de l'aspirateur (input 3 et 4) des entiers aussi. L'orientation elle doit être soit N, E, S et W. Les instructions sont des combinaisons de D, G et A.

le template est https://colorlib.com/wp/template/colorlib-regform-8/
![image.png](./image.png)

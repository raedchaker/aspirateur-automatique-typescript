class Yanport {
    constructor(x, y) {
        this.x = x;
        this.y = y;
        this.aspirateur = new Aspirateur(0, 0, 'N');
    }
    defineGrille(x, y) {
        this.x = x;
        this.y = y;
    }
    addAspirateur(x, y, orientation) {
        this.aspirateur = new Aspirateur(x, y, orientation);
    }
    move(instructions) {
        for (let e of instructions) {
            if (e == 'A') {
                this.moveAspiratuer();
            }
            else {
                this.aspirateur.rotate(e);
            }
        }
        this.aspirateur.whereAmI();
    }
    moveAspiratuer() {
        switch (this.aspirateur.orientation) {
            case 'E':
                if (this.x > this.aspirateur.x)
                    this.aspirateur.x++;
                break;
            case 'W':
                if (this.aspirateur.x > 0)
                    this.aspirateur.x--;
                break;
            case 'N':
                if (this.y > this.aspirateur.y)
                    this.aspirateur.y++;
                break;
            case 'S':
                if (this.aspirateur.y > 0)
                    this.aspirateur.y--;
                break;
        }
    }
}
class Aspirateur {
    constructor(x, y, orientation) {
        this.x = x;
        this.y = y;
        this.orientation = orientation;
    }
    rotate(sens) {
        switch (this.orientation) {
            case 'N':
                if (sens == 'D')
                    this.orientation = 'E';
                else
                    this.orientation = 'W';
                break;
            case 'E':
                if (sens == 'D')
                    this.orientation = 'S';
                else
                    this.orientation = 'N';
                break;
            case 'S':
                if (sens == 'D')
                    this.orientation = 'W';
                else
                    this.orientation = 'E';
                break;
            case 'W':
                if (sens == 'D')
                    this.orientation = 'N';
                else
                    this.orientation = 'S';
                break;
        }
    }
    whereAmI() {
        let position = 'position finale: x= ' + this.x + ' y= ' + this.y + ' orientation=' + this.orientation;
        document.getElementById('finalPosition').textContent = position;
    }
}
function calculateFinalPosition() {
    let inputs = document.querySelectorAll('input');
    let errorElement = document.getElementById('error');
    let messages = [];
    for (let input of inputs)
        input.style.borderColor = "";
    for (let input of inputs)
        if (input.value.length == 0)
            input.style.borderColor = "red";
    if (isNaN(+inputs[0].value)) {
        messages.push('grille X doit être un nombre');
        inputs[0].style.borderColor = "red";
    }
    if (isNaN(+inputs[1].value)) {
        messages.push('grille Y doit être un nombre');
        inputs[1].style.borderColor = "red";
    }
    if (isNaN(+inputs[2].value)) {
        messages.push('position X de l\'aspirateur doit être un nombre');
        inputs[2].style.borderColor = "red";
    }
    else if (+inputs[2].value > +inputs[0].value) {
        messages.push('position X de l\'aspirateur doit être inférieur a la dimension X de la grille');
        inputs[2].style.borderColor = "red";
    }
    if (isNaN(+inputs[3].value)) {
        messages.push('position Y de l\'aspirateur doit être un nombre');
        inputs[3].style.borderColor = "red";
    }
    else if (+inputs[3].value > +inputs[1].value) {
        messages.push('position Y de l\'aspirateur doit être inférieur a la dimension Y de la grille');
        inputs[3].style.borderColor = "red";
    }
    if (!(inputs[4].value == 'N' || inputs[4].value == 'E' || inputs[4].value == 'S' || inputs[4].value == 'W')) {
        messages.push('orientation doit être N, E, S ou W');
        inputs[4].style.borderColor = "red";
    }
    if (!/^[D,G,A]+$/.test(inputs[5].value)) {
        messages.push('Les instructions ne doivent contenir que des A, D et G');
        inputs[5].style.borderColor = "red";
    }
    errorElement.innerText = '';
    if (messages.length > 0) {
        errorElement.innerText = messages.join(', ');
    }
    else {
        let test = new Yanport(+inputs[0].value, +inputs[1].value);
        test.addAspirateur(+inputs[2].value, +inputs[3].value, inputs[4].value);
        test.move(inputs[5].value);
    }
}
